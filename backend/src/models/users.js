import { db } from '../database';

export const users = {
  /**
   * Get all users from DB
   * @param {*} columns what column you need to get [default: username, email]
   * @returns
   */
  async getAllUser(columns) {
    if (!columns) columns = `username, email`;

    const query = `SELECT ${columns} FROM users;`

    const data = await db.query(query);

    return data.results;
  },
  /**
   * Select user by username or email [default: username]
   * @param {*} value value to search
   * @param {*} column username or email
   */
  async getUserBySpecific(value, column) {
    if (!column) column = `username`;

    const query = `SELECT * FROM users where ${column} = ?;`;

    const data = await db.query(query, value);

    return data.results;
  },
  /**
   * Checking existed users by username and email
   * @param {*} values [username, email]
   * @returns 
   */
  async checkUserIsExist(values) {
    const query = `SELECT * FROM users where username = ? or email = ?;`;

    const data = await db.query(query, values);

    return data.results;
  },
  /**
   * Inserting new user to users table by adding an object {username: 'example'...etc}
   * @param {*} values {username: '', fullName: '', password:'', email: '', gender: 0}
   * @returns 
   */
  async inserNewUser(values) {
    const query = `INSERT INTO users SET ?;`;

    const data = await db.query(query, values);

    return data.results.insertId;
  }
}