import { registrationService } from '../services';

export const registrationController = {
  async post(req, res) {
    try {
      const userData = {
      username: req.body.username || '',
      fullName: req.body.fullName || '',
      email: req.body.email || '',
      password: req.body.password || '',
      gender: req.body.gender || '',
      passwordConfirm: req.body.passwordConfirm || ''
      }
      let data = await registrationService.newUser(userData);

      res.status(200).json(data);
    } catch (e) {
      res.status(e.status || 500).json({ message: e.message, status: e.status })
    }

  },
};