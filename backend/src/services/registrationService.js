import passwordValidator from 'password-validator';
import { users } from '../models/users';
import bcrypt from 'bcrypt';
import config from '../config';

export const registrationService = {
  async newUser(userData) {

    if (
      !userData.username ||
      !userData.fullName ||
      !userData.email    ||
      !userData.password ||
      !userData.gender   ||
      !userData.passwordConfirm
    ) {
      throw { message: 'Please fill all the fields!', status: 400 };
    } else if (userData.password !== userData.passwordConfirm) {
      throw { message: 'Confirm password is not match!', status: 400 };
    } else if (userData.password.length < 8) {
      throw { message: 'Your password is not long enough! [min 8. character]', status: 400 };
    }

    const schema = new passwordValidator();

    schema.is().min(8).is().max(100).has().uppercase().has().lowercase().has().digits()

    if (!schema.validate(userData.password)) {
      throw { message: 'The password must have min 8 character, lowerase, uppercase and a number!', status: 400 };
    }

    const sameUsers = await users.checkUserIsExist([userData.username, userData.email]);

    if (sameUsers.length > 0) {
      throw { message: 'There is already registrated account with this email or username!', status: 400 };
    }

    const salt = bcrypt.genSaltSync(config.saltRounds);
    const hashedPassword = bcrypt.hashSync(userData.password, salt);

    await users.inserNewUser({
      username: userData.username,
      fullName: userData.fullName,
      password: hashedPassword,
      gender: userData.gender,
      email: userData.email,
    })

    // email validation part, here need to implement

    return { message: "Successfull registration!", status: 201 };
  },
};