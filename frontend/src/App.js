import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Register from './Register';
/* import Login from './Login'; */

function App() {
  const [data, setData] = React.useState({
    password: '',
    passwordConfirm: '',
    showPassword: false,
    showPasswordConfirm: false,
    score: 0
  })
  // eslint-disable-next-line no-unused-vars
  const [isLoggedIn, setIsLoggedIn] = React.useState(false);


  return (
    <div className="App">
      <div>HELLO</div>
      {/* <Header/> */}
      <Routes>
        {/* <Route
          path="/login"
          element={<Login
          />}
        /> */}

        <Route
          path="/register"
          element={
            <Register data={ data } setData={ setData }/>
          }
        />

        {/* <Route
          path="/login"
          element={
            <Login setIsLoggedIn={ setIsLoggedIn } />
          }
        /> */}

        {/* <Route
          path="/register/:token"
          element={<NotImplementedPage />}
        /> */}

        {/* <Route
          path="/settings"
          element={<Settings />}
        /> */}

        {/* <Route
          path="/:userLink"
          element={
            <Main />
          }
        /> */}

        {/* <Route
          path="/"
          element={<LandingPage />}
        /> */}

        {/* <Route
          path="*"
          element={<NotImplementedPage />}
        /> */}

      </Routes>
    </div>
  );
}

export default App;
