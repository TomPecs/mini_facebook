const config = {}

config.protocol = 'http';
config.domain = `localhost`;
config.port = '8080';
config.production = false;

config.url = config.production ? `${config.protocol}://${config.domain}/` : `${config.protocol}://${config.domain}:${config.port}/`;

export default config;