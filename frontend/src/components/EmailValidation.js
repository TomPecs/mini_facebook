import { FormControl, TextField } from '@mui/material';
import React, { useState } from 'react'

export default function EmailValidation({ label, fieldName, helperText }) {

  const [value, setValue] = useState({
  password: '',
  showPassword: false,
  });

  const handleChange = (e) => {
    e.preventDefault();
  }

  return (
    <FormControl fullWidth={true} >
      <TextField
        onBlur={() => setDirty(true)}
        id={fieldName}
        label={label}
        name={fieldName}
        variant="outlined"
        size={'small'}
        helperText={helperText}
        value={value}
        InputLabelProps={{
          shrink: true,
        }}
        onChange={(e) => handleChange(e)}
      />
    </FormControl>
  )
}
