import React, { useEffect, useState} from 'react'
import Button from './components/Button';
import config from './config/config'
import FormControl from '@mui/material/FormControl';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import ViewsDatePicker from './components/ViewsDatePicker';
import zxcvbn from 'zxcvbn';
import PasswordStrength from './components/PasswordStrength';
import { FormControlLabel, FormLabel, Radio, RadioGroup } from '@mui/material';
import { errorHandler } from './middlewares/errorHandling';
import { useNavigate } from 'react-router-dom';

export default function Register({data, setData}) {
  const [errorMessage, setErrorMessage] = useState({message: '', error: true})
  const [dobDate, setDobDate] = React.useState(" 01/01/1970");
  const navigate = useNavigate();

  const handleChange = (prop) => (e) => {
    setData({ ...data, [prop]: e.target.value });
  };

  const handleClickShowPassword = () => {
    setData({
      ...data,
      showPassword: !data.showPassword,
      });
  };

  const handleClickShowPasswordConfirm = () => {
    setData({
      ...data,
      showPasswordConfirm: !data.showPasswordConfirm,
    });
  };

  const submitHandler = (e) => {
    e.preventDefault();

    if (
      !e.target.username.value ||
      !e.target.fullName.value ||
      !e.target.email.value    ||
      !e.target.gender.value   ||
      !e.target.password.value ||
      !e.target.passwordConfirm.value
      ) {
      setErrorMessage({ message: 'Please fill out all the fields.', error: true });
      } else if (e.target.password.value !== e.target.passwordConfirm.value) {
        setErrorMessage({ message: 'Confirm Password Not Match', error: true });
      } else {
        setErrorMessage({ message: '', error: true });
        let reqBody = {
          username: e.target.username.value,
          fullName: e.target.fullName.value,
          email: e.target.email.value,
          dob: (dobDate === " 01/01/1970") ? 0 : dobDate,
          gender: e.target.gender.value,
          password: e.target.password.value,
          passwordConfirm: e.target.passwordConfirm.value,
        };
        fetch(config.url + "api/register", {
          method: "POST",
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(reqBody)
        }).then(res => res.json()).then(data => {
          if (!errorHandler(data)) {
            setErrorMessage({ message: data.message, error: true })
            setTimeout(() => setErrorMessage({ message: '', error: true }), 5000);

            return;
          }

          setErrorMessage({ message: data.message, error: false })
          setTimeout(() => {
            setErrorMessage({ message: "", error: false })
            navigate('/');
          },5000)
          console.log('data', data)
        }).catch(e => {
          if (e) {
            console.log({message: 'Server error', status: 500})
        }
        })

      }
  }

  useEffect(() => {
    const pw = zxcvbn(data.password);
    if (!data.password) {
      setData({...data, score: 0})
    } else {
      setData({ ...data,
        score: pw.score + 1
      })
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data.password] )

  const handleMouseDownPassword = (e) => {
    e.preventDefault();
  };

    return (
          <div className='register-container'>
            <h1 className=''>Register:</h1>
            <div className='register-form'>
              <form className='register' onSubmit={ submitHandler } autoComplete="off" >
            <p className={ errorMessage.error? "error" : "success" } > { errorMessage.message || <br /> } </p>
                <div>
                  <FormControl fullWidth={true}>
                    <InputLabel htmlFor="component-outlined" required>Username:</InputLabel>
                    <OutlinedInput
                      id="component-outlined-username"
                      label="Username"
                      name="username"
                      type='text'
                      inputProps={{ minLength: 5, maxLength: 50}}
                      required
                    />
                  </FormControl>
                </div>
                <div>
                  <FormControl fullWidth={true}>
                    <InputLabel htmlFor="component-outlined" required>Full name:</InputLabel>
                    <OutlinedInput
                      id="component-outlined-full-name"
                      label="Full name"
                      name="fullName"
                      type='text'
                      inputProps={{ minLength: 5, maxLength: 50}}
                      required
                    />
                  </FormControl>
                </div>
                <div>
                  <FormControl fullWidth={true} >
                    <InputLabel htmlFor="component-outlined" required>Email</InputLabel>
                    <OutlinedInput
                      id="component-outlined"
                      label="Email"
                      name="email"
                      type='email'
                      required
                    />
                  </FormControl>
                </div>
                <div>
                  <FormControl>
                    <FormLabel id="gender" required>Gender</FormLabel>
                    <RadioGroup
                      row
                      aria-labelledby="gender"
                      name="gender"
                    >
                      <FormControlLabel value="0" control={<Radio />} label="Female"  />
                      <FormControlLabel value="1" control={<Radio required={true}  />} label="Male" />
                      <FormControlLabel value="2" control={<Radio />} label="Not Share" />
                    </RadioGroup>
                  </FormControl>

                </div>
                <div>
                  <ViewsDatePicker label="Date Of Birth" className="datePicker" value={dobDate} setValue={setDobDate} />
                </div>
                <div>
                  <FormControl fullWidth={true} variant="outlined">
                    <InputLabel htmlFor="outlined-adornment-password" required>Password</InputLabel>
                    <OutlinedInput
                      id="outlined-adornment-password"
                      type={data.showPassword ? 'text' : 'password'}
                      value={data.password}
                      name="password"
                      onChange={handleChange('password')}
                      required
                      title="Password must be 8 characters including 1 uppercase letter, 1 lowercase letter and numeric characters"
                      inputProps={{pattern: `^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,12}$`}}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                            edge="end"
                          >
                            {data.showPassword ? <VisibilityOff /> : <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      }
                      label="Password"
                    />
                  </FormControl>
                </div>
                  <div className="pwStrRow">
                    {data.score>= 1 && ( <div> <PasswordStrength score={data.score} /> </div> )}
                  </div>
                <div>
                  <FormControl fullWidth={true} variant="outlined">
                    <InputLabel htmlFor="outlined-adornment-password" required>Confirm password:</InputLabel>
                    <OutlinedInput
                      id="outlined-adornment-password-confirm"
                      type={data.showPasswordConfirm ? 'text' : 'password'}
                      value={data.passwordConfirm}
                      name="passwordConfirm"
                      onChange={handleChange('passwordConfirm')}
                      required
                      title="The confirm password must be same as the password."
                      inputProps={{pattern: `^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,12}$`}}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPasswordConfirm}
                            onMouseDown={handleMouseDownPassword}
                            edge="end"
                          >
                            {data.showPasswordConfirm ? <VisibilityOff /> : <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      }
                      label="PasswordConfirm"
                    />
                  </FormControl>
                </div>
                <div className='register-button'>
                  <Button type="submit" value="Submit" />
                </div>
              </form>
            </div>
          </div>
    );
  };
