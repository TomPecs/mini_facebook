export function errorHandler(data) {
  if (data.status && (data.status === 201 || data.status === 200)) return true;

  return false;
}